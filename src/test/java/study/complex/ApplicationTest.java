package study.complex;


import org.junit.Assert;
import org.junit.Test;

public class ApplicationTest {

    @Test
    public void testEquals() throws Exception {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void testNotEquals() throws Exception {
        Assert.assertNotEquals(1, 2);
    }
}
